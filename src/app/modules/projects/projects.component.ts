import { Component, OnInit } from '@angular/core';

import data from './Projects.json';


@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.scss']
})
export class ProjectsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'authorId', 'teamId', 'name', "description", "deadline", "createdAt"];
  dataSource = data;
  
  public show: boolean = true;
  
  constructor() { }

  ngOnInit(): void {
  }

}
