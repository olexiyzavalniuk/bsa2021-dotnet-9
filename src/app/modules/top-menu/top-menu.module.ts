import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopMenuComponent } from './top-menu.component'
import { SharedModule } from '../shared/shared.module';
import { AppRoutingModule } from '../../app-routing.module';

@NgModule({
  declarations: [TopMenuComponent],
  imports: [
    CommonModule,
    SharedModule,
    AppRoutingModule
  ],
  exports: [TopMenuComponent]
})
export class TopMenuModule { }
