import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-top-menu',
  templateUrl: './top-menu.component.html',
  styleUrls: ['./top-menu.component.scss']
})
export class TopMenuComponent implements OnInit {

  isMenuOpened: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }

  onMenuToggle() {
    this.isMenuOpened = true;
    console.log('toggle')
  }

}
