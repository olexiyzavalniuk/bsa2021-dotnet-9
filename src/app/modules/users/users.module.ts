import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserListComponent } from './components/user-list/user-list.component';
import { EditUserDialogComponent } from './components/edit-user-dialog/edit-user-dialog.component';
import { DeleteUserConfirmationDialog } from './components/delete-user-confirmation-dialog/delete-user-confirmation-dialog.component';

import {  ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';

@NgModule({
  declarations: [
    UserListComponent,
    EditUserDialogComponent,
    DeleteUserConfirmationDialog
  ],
  exports: [
    UserListComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule
  ],
  entryComponents: [
    EditUserDialogComponent,
    DeleteUserConfirmationDialog
  ]
})
export class UsersModule { }
