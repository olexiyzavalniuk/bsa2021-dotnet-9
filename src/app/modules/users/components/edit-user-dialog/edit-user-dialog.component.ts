import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, Validators, FormGroup, FormControl, AbstractControl } from '@angular/forms';

@Component({
  selector: 'app-edit-user-dialog',
  templateUrl: './edit-user-dialog.component.html',
  styleUrls: ['./edit-user-dialog.component.scss']
})
export class EditUserDialogComponent implements OnInit {

  editUserForm: FormGroup;

  get firstNameFormControl() {
    return this.editUserForm.get('firstName') as FormControl;
  }

  get lastNameFormControl() {
    return this.editUserForm.get('lastName') as FormControl;
  }

  get emailFormControl() {
    return this.editUserForm.get('email') as FormControl;
  }

  constructor(
    private dialogRef: MatDialogRef<EditUserDialogComponent>,
    private formBuilder: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
    this.editUserForm = this.formBuilder.group({
      firstName: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50)
        ]],
      lastName: ['',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.maxLength(50)
        ]],
      email: ['', [
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ]]
    });
  }


  close() {
    this.dialogRef.close();
  }

}
