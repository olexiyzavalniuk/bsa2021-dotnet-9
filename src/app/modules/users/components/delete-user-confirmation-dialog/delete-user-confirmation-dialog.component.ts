import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-delete-user-confirmation-dialog',
  templateUrl: './delete-user-confirmation-dialog.component.html',
  styleUrls: ['./delete-user-confirmation-dialog.component.scss']
})
export class DeleteUserConfirmationDialog implements OnInit {

  constructor(
    private dialogRef: MatDialogRef<DeleteUserConfirmationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }


  close() {
    this.dialogRef.close();
  }

}
