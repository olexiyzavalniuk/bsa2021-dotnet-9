import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { DeleteUserConfirmationDialog } from '../delete-user-confirmation-dialog/delete-user-confirmation-dialog.component';
import { EditUserDialogComponent } from '../edit-user-dialog/edit-user-dialog.component';

import data from './Users.json';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.scss']
})
export class UserListComponent implements OnInit {
  displayedColumns: string[] = ['Id', 'TeamId', 'FirstName', 'LastName', "Email", "RegisteredAt", "BirthDay", "Actions"];
  dataSource = data;
  isUsersLoading = false;

  constructor(private dialogService: MatDialog) { }

  ngOnInit(): void {

  }

  onEditUser() {
    this.dialogService.open(EditUserDialogComponent, {
      minWidth: 600
    });
  }

  onDeleteUser() {
    this.dialogService.open(DeleteUserConfirmationDialog, {
      minWidth: 400
    })
  }
}
