import { Component, OnInit } from '@angular/core';

import data from './Tasks.json';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss']
})
export class TasksComponent implements OnInit {

  displayedColumns: string[] = ['id', 'projectId', 'performerId', 'name', 
  "description", "state", "createdAt", "finishedAt"];
  dataSource = data;
  
  constructor() { }

  ngOnInit(): void {
  }

}
