import { Component, OnInit } from '@angular/core';

import data from './Teams.json';

 @Component({
   selector: 'app-teams',
   templateUrl: './teams.component.html',
   styleUrls: ['./teams.component.scss']
 })
 export class TeamsComponent implements OnInit {

  displayedColumns: string[] = ['id', 'name', 'createdAt'];
  dataSource = data;

  constructor() { }

  ngOnInit(): void {
  }

}

