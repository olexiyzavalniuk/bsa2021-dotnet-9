import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TeamsComponent } from './teams.component';
import { SharedModule } from '../shared/shared.module';


@NgModule({
  declarations: [
    TeamsComponent
  ],
  exports: [
    TeamsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ]
})
export class TeamsModule { }
