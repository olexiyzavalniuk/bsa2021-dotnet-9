import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProjectsComponent } from './modules/projects/projects.component';
import { TeamsComponent } from './modules/teams/teams.component';
import { UserListComponent } from './modules/users/components/user-list/user-list.component';
import { TasksComponent } from './modules/tasks/tasks.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'projects'
  },
  {
    path: 'projects',
    component: ProjectsComponent
  },
  {
    path: 'teams',
    component: TeamsComponent
  },
  {
    path: 'users',
    component: UserListComponent
  },
  {
    path: 'tasks',
    component: TasksComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
